from xia_fields import StringField, IntField, FloatField, BooleanField
from xia_engine import EmbeddedDocument, Document, EmbeddedDocumentField, ListField


class Address(EmbeddedDocument):
    street: str = StringField(description='Street name and number', required=True)
    city: str = StringField(description='City', required=True)
    state: str = StringField(description='State or province', required=True)
    zip: str = StringField(description='Postal or ZIP code', required=True)


class Customer(Document):
    _key_fields = ['customer_id']
    customer_id: str = StringField(description='Unique identifier for the customer', required=True)
    name: str = StringField(description='Name of the customer', required=True)
    shipping_address: object = EmbeddedDocumentField(document_type=Address)
    billing_address: object = EmbeddedDocumentField(document_type=Address)
    payment_methods: list = ListField(StringField(), description="List of customer's payment methods")
    order_history: list = ListField(StringField(), description="List of customer's past orders")
