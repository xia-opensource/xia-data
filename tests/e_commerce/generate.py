import json
import pytest
from xia_compiler_python import PythonCompiler


def generate_py():
    with open("tests/e_commerce/schemas/customer.json") as fp:
        customer_schema = json.load(fp)
    print(PythonCompiler.generate_python("customer", customer_schema, key_fields=["customer_id"]))


if __name__ == '__main__':
    generate_py()
